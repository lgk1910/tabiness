import cv2
import os
import glob
import numpy as np

def is_full_border_process(horizontal_lines, vertical_lines, img_path, segment):
    
    basename = os.path.basename(img_path)
    img = cv2.imread(img_path)
    height,width = img.shape[0], img.shape[1]
    vertical_counter = 0
    horizontal_counter = 0
    is_full_border = False
    
    if len(horizontal_lines) != 0:
        #HORIZONTAL
        horizontal_segment = int(height/segment)
        # print(horizontal_lines[0]) #[[[  9 342]] [[593 342]]]
        horizontal_points=[]
        # for id,line in enumerate(horizontal_lines):
        #     if id%2==0 and id<len(horizontal_lines)-1:
        #         if horizontal_lines[id+1][0][0][1]==line[0][0][1]:
        #             if abs(horizontal_lines[id+1][0][0][0]-line[0][0][0]) >= width*0.6:
        
        for id,line in enumerate(horizontal_lines):
            horizontal_points.append(line[0][0][1])

        curr_segment = 0
        while curr_segment < height:
            for point in horizontal_points:
                if point >= curr_segment and point <= curr_segment + horizontal_segment:
                    horizontal_counter += 1
                    break
            curr_segment += horizontal_segment
        
        #debug
        img_copy = img.copy()
        for c in horizontal_points:
            cv2.drawContours(img_copy, [np.array([[0,c], [50,c], [100,c]])], -1, (0,255,0), 5)
        curr_segment = 1
        while curr_segment < height:
            cv2.drawContours(img_copy, [np.array([[0,curr_segment], [50,curr_segment], [100,curr_segment]])], -1, (255,255,0), 5)
            curr_segment += horizontal_segment
        cv2.imwrite('draw_horizontal/'+basename, img_copy)
    
    #--------------------------------------
    
    #VERTICAL  
    if len(vertical_lines) != 0:
        vertical_segment = int(width/segment)
        # print(len(vertical_lines))
        # print(vertical_lines[0]) #[[[  9 342]] [[593 342]]]
        vertical_points=[]
        if len(vertical_lines)==1:
            vertical_lines = vertical_lines[0]
            for line in vertical_lines:
                vertical_points.append(line[0][0])
        else:
            # for id,line in enumerate(vertical_lines):
            #     if id%2==0 and id<len(vertical_lines)-1:
            #         if vertical_lines[id+1][0][0][0]==line[0][0][0]:
            #             if abs(vertical_lines[id+1][0][0][1]-line[0][0][1]) >= height*0.6:
            
            for line in vertical_lines:
                vertical_points.append(line[0][0][0])
        
        # print(vertical_points)
        
        curr_segment = 0
        while curr_segment < width:
            for point in vertical_points:
                if point >= curr_segment and point <= curr_segment + vertical_segment:
                    vertical_counter += 1
                    break
            curr_segment += vertical_segment
        
        #debug
        img_copy = img.copy()
        for c in vertical_points:
            cv2.drawContours(img_copy, [np.array([[c,0], [c,50], [c,100]])], -1, (0,255,0), 5) #green
        curr_segment = 1
        while curr_segment < width:
            cv2.drawContours(img_copy, [np.array([[curr_segment,0], [curr_segment,50], [curr_segment,100]])], -1, (255,255,0), 5)
            curr_segment += vertical_segment
        cv2.imwrite('draw_vertical/'+basename, img_copy)
    
    
    if vertical_counter >= np.floor(0.6*segment) and horizontal_counter >= np.floor(0.6*segment):
        is_full_border = True
        
    else:
        is_full_border = False
        # print('vertical: ', vertical_counter, 'horizontal: ', horizontal_counter, basename)
    
    return is_full_border

def is_full_border_process_image(horizontal_lines, vertical_lines, img, segment, output_name='output.jpg'):
    height,width = img.shape[0], img.shape[1]
    vertical_counter = 0
    horizontal_counter = 0
    is_full_border = False
    
    if len(horizontal_lines) != 0:
        #HORIZONTAL
        horizontal_segment = int(height/segment)
        # print(horizontal_lines[0]) #[[[  9 342]] [[593 342]]]
        horizontal_points=[]
        # for id,line in enumerate(horizontal_lines):
        #     if id%2==0 and id<len(horizontal_lines)-1:
        #         if horizontal_lines[id+1][0][0][1]==line[0][0][1]:
        #             if abs(horizontal_lines[id+1][0][0][0]-line[0][0][0]) >= width*0.6:
        
        for id,line in enumerate(horizontal_lines):
            horizontal_points.append(line[0][0][1])

        curr_segment = 0
        while curr_segment < height:
            for point in horizontal_points:
                if point >= curr_segment and point <= curr_segment + horizontal_segment:
                    horizontal_counter += 1
                    break
            curr_segment += horizontal_segment
        
        #debug
        # img_copy = img.copy()
        # for c in horizontal_points:
        #     cv2.drawContours(img_copy, [np.array([[0,c], [50,c], [100,c]])], -1, (0,255,0), 5)
        # curr_segment = 1
        # while curr_segment < height:
        #     cv2.drawContours(img_copy, [np.array([[0,curr_segment], [50,curr_segment], [100,curr_segment]])], -1, (255,255,0), 5)
        #     curr_segment += horizontal_segment
        # cv2.imwrite('draw_horizontal/'+output_name, img_copy)
    
    #--------------------------------------
    
    #VERTICAL  
    if len(vertical_lines) != 0:
        vertical_segment = int(width/segment)
        # print(len(vertical_lines))
        # print(vertical_lines[0]) #[[[  9 342]] [[593 342]]]
        vertical_points=[]
        if len(vertical_lines)==1:
            vertical_lines = vertical_lines[0]
            for line in vertical_lines:
                vertical_points.append(line[0][0])
        else:
            # for id,line in enumerate(vertical_lines):
            #     if id%2==0 and id<len(vertical_lines)-1:
            #         if vertical_lines[id+1][0][0][0]==line[0][0][0]:
            #             if abs(vertical_lines[id+1][0][0][1]-line[0][0][1]) >= height*0.6:
            
            for line in vertical_lines:
                vertical_points.append(line[0][0][0])
        
        # print(vertical_points)
        
        curr_segment = 0
        while curr_segment < width:
            for point in vertical_points:
                if point >= curr_segment and point <= curr_segment + vertical_segment:
                    vertical_counter += 1
                    break
            curr_segment += vertical_segment
        
        #debug
        # img_copy = img.copy()
        # for c in vertical_points:
        #     cv2.drawContours(img_copy, [np.array([[c,0], [c,50], [c,100]])], -1, (0,255,0), 5) #green
        # curr_segment = 1
        # while curr_segment < width:
        #     cv2.drawContours(img_copy, [np.array([[curr_segment,0], [curr_segment,50], [curr_segment,100]])], -1, (255,255,0), 5)
        #     curr_segment += vertical_segment
        # cv2.imwrite('draw_vertical/'+basename, img_copy)
    
    
    if vertical_counter >= np.floor(0.6*segment) and horizontal_counter >= np.floor(0.6*segment):
        is_full_border = True
        
    else:
        is_full_border = False
        # print('vertical: ', vertical_counter, 'horizontal: ', horizontal_counter, basename)
    
    return is_full_border

def is_full_border(img_path, output_path):
    """
    Input: 
        img_path
        output_path: folder to save visualizations, if None then no visual is done
    Output:
        Boolean value: True --- full border
                        False --- partially-bordered/non-bordered
    """
    basename = os.path.basename(img_path)
    image = cv2.imread(img_path)
    result = image.copy()
    gray = cv2.cvtColor(image,cv2.COLOR_BGR2GRAY)
    #better define borders
    kernel = np.ones((3, 3), np.uint8)
    gray = cv2.erode(gray, kernel, iterations=1)
    gray = cv2.dilate(gray, kernel, iterations=1)
    #
    thresh = cv2.threshold(gray, 0, 255, cv2.THRESH_BINARY_INV + cv2.THRESH_OTSU)[1]

    # Remove horizontal lines
    horizontal_kernel = cv2.getStructuringElement(cv2.MORPH_RECT, (40,1))
    remove_horizontal = cv2.morphologyEx(thresh, cv2.MORPH_OPEN, horizontal_kernel, iterations=2)
    cnts = cv2.findContours(remove_horizontal, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
    cnts_horizontal = cnts[0] if len(cnts) == 2 else cnts[1]
    
    # print('HORIZONTAL')
    # for id,line in enumerate(cnts_horizontal):
    #    print(line[0][0],end=' ')
            
    cnts_horizontal = [c for c in cnts_horizontal if cv2.arcLength(c, True) >= 0.4*image.shape[1]]
    # print('\n After trim')
    # for id,line in enumerate(cnts_horizontal):
    #    print(line[0][0],end=' ')
    
       
    for c in cnts_horizontal:
        cv2.drawContours(result, [c], -1, (0,255,255), 5)

    # Remove vertical lines
    vertical_kernel = cv2.getStructuringElement(cv2.MORPH_RECT, (1,40))
    remove_vertical = cv2.morphologyEx(thresh, cv2.MORPH_OPEN, vertical_kernel, iterations=2)
    cnts = cv2.findContours(remove_vertical, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
    cnts_vertical = cnts[0] if len(cnts) == 2 else cnts[1]
    
    # print('\n VERTICAL')
    # for id,line in enumerate(cnts_vertical):
    #    print(line[0][0],end=' ')
       
    cnts_vertical = [c for c in cnts_vertical if cv2.arcLength(c, True) >= 0.4*image.shape[0]]
    # print('\n After trim')
    # for id,line in enumerate(cnts_vertical):
    #    print(line[0][0],end=' ')
    # print('\n ------------------------------------- \n')
       
    for c in cnts_vertical:
        cv2.drawContours(result, [c], -1, (0,255,255), 5)
    
    # print(basename)
    # if is_full_border_process(cnts_horizontal, cnts_vertical, img_path, 5):
    #     print('is full border')
    # else:
    #     print('NOT')
    # print('-------------------------------------------')
 
    if output_path is not None:
        output_path += '/' + basename
        cv2.imwrite(output_path, result)

    return is_full_border_process(cnts_horizontal, cnts_vertical, img_path, 5)

def is_full_border_image(image, output_path):
    """
    Input: 
        img_path
        output_path: folder to save visualizations, if None then no visual is done
    Output:
        Boolean value: True --- full border
                        False --- partially-bordered/non-bordered
    """
    result = image.copy()
    gray = cv2.cvtColor(image,cv2.COLOR_BGR2GRAY)
    #better define borders
    kernel = np.ones((3, 3), np.uint8)
    gray = cv2.erode(gray, kernel, iterations=1)
    gray = cv2.dilate(gray, kernel, iterations=1)
    #
    thresh = cv2.threshold(gray, 0, 255, cv2.THRESH_BINARY_INV + cv2.THRESH_OTSU)[1]

    # Remove horizontal lines
    horizontal_kernel = cv2.getStructuringElement(cv2.MORPH_RECT, (40,1))
    remove_horizontal = cv2.morphologyEx(thresh, cv2.MORPH_OPEN, horizontal_kernel, iterations=2)
    cnts = cv2.findContours(remove_horizontal, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
    cnts_horizontal = cnts[0] if len(cnts) == 2 else cnts[1]
    
    # print('HORIZONTAL')
    # for id,line in enumerate(cnts_horizontal):
    #    print(line[0][0],end=' ')
            
    cnts_horizontal = [c for c in cnts_horizontal if cv2.arcLength(c, True) >= 0.4*image.shape[1]]
    # print('\n After trim')
    # for id,line in enumerate(cnts_horizontal):
    #    print(line[0][0],end=' ')
    
       
    for c in cnts_horizontal:
        cv2.drawContours(result, [c], -1, (0,255,255), 5)

    # Remove vertical lines
    vertical_kernel = cv2.getStructuringElement(cv2.MORPH_RECT, (1,40))
    remove_vertical = cv2.morphologyEx(thresh, cv2.MORPH_OPEN, vertical_kernel, iterations=2)
    cnts = cv2.findContours(remove_vertical, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
    cnts_vertical = cnts[0] if len(cnts) == 2 else cnts[1]
    
    # print('\n VERTICAL')
    # for id,line in enumerate(cnts_vertical):
    #    print(line[0][0],end=' ')
       
    cnts_vertical = [c for c in cnts_vertical if cv2.arcLength(c, True) >= 0.4*image.shape[0]]
    # print('\n After trim')
    # for id,line in enumerate(cnts_vertical):
    #    print(line[0][0],end=' ')
    # print('\n ------------------------------------- \n')
       
    for c in cnts_vertical:
        cv2.drawContours(result, [c], -1, (0,255,255), 5)
    
    # print(basename)
    # if is_full_border_process(cnts_horizontal, cnts_vertical, img_path, 5):
    #     print('is full border')
    # else:
    #     print('NOT')
    # print('-------------------------------------------')
 
    if output_path is not None:
        cv2.imwrite(output_path, result)

    return is_full_border_process_image(cnts_horizontal, cnts_vertical, image, 5)
    
    

#Driver code
if __name__ == '__main__':   
    """
    Input: 
        img_path
        output_path: folder to save visualizations, if None then no visual is done
    Output:
        Boolean value: True --- full border
                        False --- partially-bordered/non-bordered
    """
    
    output_path = 'output'
    i = 0
    for item in glob.glob('full/*'):
        if is_full_border(item, output_path=output_path) == False:
            print('Error', item)
        # i +=1
        # if i ==4:
        #     break

    for item in glob.glob('noborder/*'):
        if is_full_border(item, output_path=output_path) == True:
            print('Error', item)
    #     break
            # pass
            
            
        
