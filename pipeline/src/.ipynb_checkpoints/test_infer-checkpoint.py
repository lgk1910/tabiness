import cv2
from glob import glob
import os
import sys
sys.path.append("../tablenet")
from tablenet import tablenet
from table_models import table_net
import easyocr

if __name__ == '__main__':
    model  = table_net((None, None, 3), 2)
    model.load_weights('/home/ec2-user/table_transformer_khoi/tablenet/table_line_models/table-line.h5')
    # tf.saved_model.save(model, './saved_model/table_line')
    reader = easyocr.Reader(['vi','en'], gpu=False)
    # reader = AkaOCR(method='triton', output='tmp', recog_exp_name='exp_recog_vi', detect_exp_name='detect_easy')
    # img = cv2.imread('/home/ec2-user/pipeline_table_extraction/table_classification/output/output2.png')

    detect_line = tablenet(model=model,engine='easyocr',reader=reader,size = (1024,1024))
    # reader = AkaOCR(method='triton',output='./output/tmp',recog_exp_name='exp_recog_vi',detect_exp_name='detec_easy')
    
    # detect_line = table_line(model=None,engine='akaocr',reader=None,size = (1024,1024), method='pth')

    img_paths = glob('/home/ec2-user/pipeline_table_extraction/split_merge/fs_data_test/table_0.png')
    for i, img_path in enumerate(img_paths):
        ceilboxes, boxes, img_  = detect_line.infer(cv2.imread(img_path))
        basename = os.path.basename(img_path)
        cv2.imwrite(f'./output/{basename}', img_)
        print(f'{i} processed successfully')

    # ceilboxes, boxes, img_ = detect_line.infer(img)
    # cv2.imwrite('./output/output.png', img_)

    # outer = detect_line.ocr(img)
    # dataframe = pd.DataFrame(outer)
    # dataframe.to_csv('/home/ec2-user/pipeline_table_extraction/output/csv/testt.csv', index=False)
    # print(outer)