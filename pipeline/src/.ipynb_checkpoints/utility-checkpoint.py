import argparse

def get_args():
    parser = argparse.ArgumentParser()

    parser.add_argument('--det_config_file',
                        required=True,
                        help="Filepath to the config containing the args")
    parser.add_argument('--rec_config_file',
                        required=True,
                        help="Filepath to the config containing the args")
    parser.add_argument('--backbone',
                        default='resnet18',
                        help="Backbone for the model")
    parser.add_argument('--image_path', help="Input image path")
    parser.add_argument('--thresh',
                        type=float,
                        default=0.5,
                        help="Threshold for the certainty of predictions")
    parser.add_argument('--lang', nargs='+', choices=['en', 'vi'], required=True,
                        help='the language to use (either "en" or "vi" or both)')
    parser.add_argument('--output_dir', default='../output', type=str)
    parser.add_argument('--det_model_load_path', help="The path to trained model")
    parser.add_argument('--rec_model_load_path', help="The path to trained model")
    parser.add_argument('--load_weights_only', action='store_true')
    parser.add_argument('--debug', action='store_true')
    parser.add_argument('--device')

    return parser.parse_args()

import sys

class BoundingBox:
    def __init__(self, 
                left, 
                top, 
                right, 
                bot,):
        self.left = left
        self.top = top
        self.right = right
        self.bot = bot
        self.area = abs((right-left)*(bot-top))

    def iou(self, bb: "BoundingBox"):
        x1 = max(self.left, bb.left)
        x2 = min(self.right, bb.right)
        y1 = max(self.top, bb.top)
        y2 = min(self.bot, bb.bot)
        intersection = abs((x2-x1)*(y2-y1))
        try:
            return intersection / (self.area + bb.area - intersection)
        except:
            return sys.float_info.max

import re
import cv2
import numpy as np

def postprocess_ocr_output(output_text):
    # Find all indices of 'C'
    indices_object = re.finditer(pattern='C', string=output_text)
    indices = [index.start() for index in indices_object]
    new_str_lst = list(output_text)
    for idx in indices:
        if idx == 0:
            continue
        if new_str_lst[idx-1] in ['1', '2', '3', '4', '5', '6', '7', '8', '9', '0']:
            new_str_lst[idx] = '0'
    output_text = ''.join(new_str_lst)
    return output_text

def remove_lines(cell_img):
    # Remove horizontal lines
    if cell_img is None:
        print(f'cell_img is None!!!')
    try:
        gray = cv2.cvtColor(cell_img, cv2.COLOR_BGR2GRAY)
    except:
        print(f'cell_img type: {type(cell_img), cell_img}')
        
    #better define borders
    kernel = np.ones((3, 3), np.uint8)
    gray = cv2.erode(gray, kernel, iterations=1)
    gray = cv2.dilate(gray, kernel, iterations=1)
    thresh = cv2.threshold(gray, 0, 255, cv2.THRESH_BINARY_INV + cv2.THRESH_OTSU)[1]
    
    # # Detect horizontal lines
    # horizontal_kernel = cv2.getStructuringElement(cv2.MORPH_RECT, (30,1))
    # remove_horizontal = cv2.morphologyEx(thresh, cv2.MORPH_OPEN, horizontal_kernel, iterations=2)
    # cnts = cv2.findContours(remove_horizontal, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
    # cnts_horizontal = cnts[0] if len(cnts) == 2 else cnts[1]
    # cnts_horizontal = [c for c in cnts_horizontal if cv2.arcLength(c, True) >= 0.6*cell_img.shape[1]]

    # Detect vertical lines
    vertical_kernel = cv2.getStructuringElement(cv2.MORPH_RECT, (1,30))
    remove_vertical = cv2.morphologyEx(thresh, cv2.MORPH_OPEN, vertical_kernel, iterations=2)
    cnts = cv2.findContours(remove_vertical, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
    cnts_vertical = cnts[0] if len(cnts) == 2 else cnts[1]
    cnts_vertical = [c for c in cnts_vertical if cv2.arcLength(c, True) >= 0.6*cell_img.shape[0]]
    
    # # Erase horizontal lines
    # for c in cnts_horizontal:
    #     # print('horizontalllllllllllllllllllll')
    #     # print(f'c: {c}')
    #     line_thickness = abs(c[1][0][1] - c[0][0][1])+1
    #     # print(f'hline width: {line_thickness}')
    #     cv2.drawContours(cell_img, [c], -1, (255, 255,255), line_thickness)
    
    # # Erase vertical lines
    for c in cnts_vertical:
        # print('verticallllllllllll')
        # print(f'c: {c}')
        try:
            line_thickness = abs(c[2][0][0] - c[0][0][0])
            # print(f'vline width: {line_thickness}')
        except:
            line_thickness = abs(c[1][0][0] - c[0][0][0])
            # print(f'vline width: {line_thickness}')

        cv2.drawContours(cell_img, [c], -1, (255, 255, 255), line_thickness)
    return cell_img