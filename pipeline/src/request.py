import requests

# Make a GET request to the `/` endpoint 
response = requests.get("http://127.0.0.1:5000/")  
print(response.text)  # Prints {"message": "Hello, world!" }

# Make a GET request to the `/info` endpoint 
response = requests.get("http://127.0.0.1:5000/info")  
print(response.text)  # Prints {"name": "John", "age": 30}