from flask import Flask
app = Flask(__name__)

@app.route("/")
def home():
    return {"message": "Hello, world!" }

@app.route("/info") 
def info():
    return {"name": "John", "age": 30}

if __name__ == "__main__": 
    app.run(debug=True)