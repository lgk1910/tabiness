import os
import json
from datetime import datetime
import string
import sys
import random
import numpy as np
import cv2
import glob
from tqdm import tqdm
import sys
import pandas as pd
import time
from math import ceil

sys.path.append("../tableTrans/data")
sys.path.append("../tableTrans/source")
sys.path.append("../tablenet")
sys.path.append("../table_classify")

# from tablenet import tablenet
from table_models import table_net
from tablenet_utils import minAreaRectbox, measure, draw_lines,draw_boxes_v2,letterbox_image, get_table_line, adjust_lines, line_to_line,draw_boxes,minAreaRectbox_v2,Sortlayer
import easyocr
from table_classify import is_full_border, is_full_border_image

import torch
from torchvision.transforms import functional as F
from engine import evaluate, train_one_epoch
import data.transforms as R

# New
from data.loader import get_data
from modeling.model import get_model
import utils.misc as utils
from utils.early_stopping import EarlyStopper
from utility import get_args, BoundingBox, postprocess_ocr_output, remove_lines
# import pytesseract

# import for quantization
from torch.quantization import QuantStub, DeQuantStub, prepare, convert 
from torchvision.io import read_image

class TableTransformer:
    def __init__(self, det_args, device, class_map, model_type='det'):
        model, criterion, postprocessors = get_model(det_args, device)
        self.model = model
        self.postprocessors = postprocessors
        self.device = device
        self.class_map = class_map
        self.model_type = model_type
        
    def rescale_img(self, img, fraction=2):
        width = int(img.shape[1] * fraction)
        height = int(img.shape[0] * fraction)
        dim = (width, height)
        
        # resize image
        resized = cv2.resize(img, dim, interpolation = cv2.INTER_AREA)
        return resized

    def bb_intersection_over_union(self, boxA, boxB):
        # determine the (x, y)-coordinates of the intersection rectangle
        xA = max(boxA[0], boxB[0])
        yA = max(boxA[1], boxB[1])
        xB = min(boxA[2], boxB[2])
        yB = min(boxA[3], boxB[3])
        # compute the area of intersection rectangle
        interArea = abs(max((xB - xA, 0)) * max((yB - yA), 0))
        if interArea == 0:
            return 0, 0, 0
        # compute the area of both the prediction and ground-truth
        # rectangles
        boxAArea = abs((boxA[2] - boxA[0]) * (boxA[3] - boxA[1]))
        boxBArea = abs((boxB[2] - boxB[0]) * (boxB[3] - boxB[1]))

        # compute the intersection over union by taking the intersection
        # area and dividing it by the sum of prediction + ground-truth
        # areas - the interesection area
        iou = interArea / float(boxAArea)

        # return the intersection over union value
        return boxAArea, boxBArea, iou
    
    def infer(self, args, image, output_filename='output', timing=None):
        output_dir = args.output_dir
        if not os.path.exists(output_dir):
            os.makedirs(output_dir)
            
        normalize = R.Normalize([0.485, 0.456, 0.406], [0.229, 0.224, 0.225])
        h, w, _ = image.shape
        print(f'w, h: {w} {h}')
        fraction = 1
        img = image
        # if (w > h and w > 3000 and w < 5000) or (h > w and h > 3000 and h < 5000):
        #     fraction = 0.33
        #     img = self.rescale_img(image, fraction)
        #     print(f'new img shape {img.shape}')
        # elif (w > h and w > 5000 and w < 7000) or (h > w and h > 5000 and h < 7000):
        #     fraction = 0.2
        #     img = self.rescale_img(image, fraction)
        #     print(f'new img shape {img.shape}')
        # elif (w >= 7000) or (h >= 7000):
        #     fraction = 0.15
        #     img = self.rescale_img(image, fraction)
        #     print(f'new img shape {img.shape}') 
        # else:
        #     img = image
                
        img_tensor = normalize(F.to_tensor(img))[0]
        img_tensor = torch.unsqueeze(img_tensor, 0).to(self.device)
        print(f'img_tensor shape: {img_tensor.shape}')
        outputs = None
        if timing is not None:
            if self.model_type == 'det':
                timing['start_det_infer_time'] = time.time()
            else:
                timing['start_rec_infer_time'] = time.time()
        with torch.no_grad():
            outputs = self.model(img_tensor)
        if timing is not None:
            if self.model_type == 'det':
                timing['end_det_infer_time'] = time.time()
            else:
                timing['end_rec_infer_time'] = time.time()
            
        image_size = torch.unsqueeze(torch.as_tensor([int(h), int(w)]), 0).to(self.device)
        results = self.postprocessors["bbox"](outputs, image_size)[0]
        # print(results)
        class_map = get_class_map()
        ltrb_label = ''
        predictions = []
        if args.debug is True:
            for idx, score in enumerate(results["scores"].tolist()):
                if score < args.thresh:
                    continue

                xmin, ymin, xmax, ymax = list(map(int, results["boxes"][idx]))
                
                # Padding table
                if self.model_type == 'det':
                    xmin = max(xmin - img.shape[1]//20, 0)
                    xmax = min(xmax + img.shape[1]//20, img.shape[1])
                    ymin = max(ymin - img.shape[0]//20, 0)
                    ymax = min(ymax + img.shape[0]//20, img.shape[0])
                
                predictions.append({
                    'bbs': [xmin, ymin, xmax, ymax],
                    'label': class_map[results["labels"][idx].item()],
                    'score': score 
                })
                ltrb_label += f'{class_map[results["labels"][idx].item()]} {score} {int(xmin/fraction)} {int(ymin/fraction)} {int(xmax/fraction)} {int(ymax/fraction)}\n'
                sub_img = img[ymin:ymax, xmin:xmax]
                if self.model_type == 'det':
                    cv2.rectangle(image, (xmin, ymin), (xmax, ymax), (0, 255, 0), 2)
                    print(f"Saved path: {os.path.join(args.output_dir, output_filename, 'det')}")
                    if not os.path.exists(os.path.join(args.output_dir, output_filename, 'det')):
                        os.makedirs(os.path.join(args.output_dir, output_filename, 'det'))
                    cv2.imwrite(os.path.join(args.output_dir, output_filename, 'det', f'table_{idx}.jpg'), image)
            results["debug_image"] = image
            # if self.model_type == 'det':
            #     with open(os.path.join(args.output_dir, output_filename, 'det', f'{output_filename}.txt'), 'w') as f:
            #         f.write(ltrb_label)
            # else:
            #     with open(os.path.join(args.output_dir, output_filename, 'rec', f'{output_filename}.txt'), 'w') as f:
            #         f.write(ltrb_label)
            # cv2.imwrite(os.path.join(output_dir, f'{output_filename}.jpg'), image)
        else:
            for idx, score in enumerate(results["scores"].tolist()):
                if score < args.thresh:
                    continue

                xmin, ymin, xmax, ymax = list(map(int, results["boxes"][idx]))
                
                # Padding table
                if self.model_type == 'det':
                    xmin = max(xmin - img.shape[1]//20, 0)
                    xmax = min(xmax + img.shape[1]//20, img.shape[1])
                    ymin = max(ymin - img.shape[0]//20, 0)
                    ymax = min(ymax + img.shape[0]//20, img.shape[0])
                
                predictions.append({
                    'bbs': [xmin, ymin, xmax, ymax],
                    'label': class_map[results["labels"][idx].item()],
                    'score': score 
                })
                ltrb_label += f'{class_map[results["labels"][idx].item()]} {score} {int(xmin/fraction)} {int(ymin/fraction)} {int(xmax/fraction)} {int(ymax/fraction)}\n'
                sub_img = img[ymin:ymax, xmin:xmax]
            results["debug_image"] = image
        return predictions
    
    def quantize(self, calibration_inputs, output_dir, filename='model.pt'):
        for layer in self.model.transformer.layers:
            if layer.weight.device == torch.device('cpu'):
                print(f'{layer} is on CPU!')
    
        # Add quantization stubs 
        self.model.quant = QuantStub()
        self.model.dequant = DeQuantStub()

        # Specify quantization configuration 
        self.model.qconfig = torch.quantization.default_qconfig

        # Prepare model for calibration 
        prepare(self.model, inplace=True) 

        # Create calibration input (e.g. sample images) 
        cal_input = calibration_inputs  

        # Switch to eval mode 
        self.model.eval()

        # Calibrate on sample input 
        with torch.no_grad():
            _ = self.model(cal_input)

        # Convert model to quantized version 
        quant_model = convert(self.model)

        # Export quantized model for deployment
        traced_model = torch.jit.trace(quantized_model, example_input) # Export using torch.jit.trace
        if not os.path.exists(output_dir):
            os.makedirs(output_dir)
        
        save_filepath = os.path.join(output_dir, filename)
        traced_model.save(save_filepath)
        print(f'Quantized model saved path: {save_filepath}')
        
        # Test inference
        print(f'Testing quantized model')
        self.model = torch.jit.load(save_filepath)
        test_outputs = self.model(cal_input)
        print(f'Test passed! Model quantized successfully')
    
    def refine_recog_rows(self, image, rows, tolerance=2):
        for row in rows:
            best_sum = np.sum(image[row['bbs'][1],:,:])
            best_row_offset = row['bbs'][1]
            # Check whether there are potential blank line above
            potential_blank_line_above = False
            for line_offset in range(row['bbs'][1] - (tolerance - 1), row['bbs'][1]):
                if np.sum(image[line_offset,:,:]) < best_sum*0.2:
                    potential_blank_line_above = True
            if potential_blank_line_above:
                next_line_offset = row['bbs'][1] - (tolerance - 1)
            else:
                next_line_offset = row['bbs'][1] + 1
            # print(f'potential_blank_line_above: {potential_blank_line_above}')
            tol_count = 0
            while True:
                next_line_sum = np.sum(image[next_line_offset,:,:])
                if  next_line_sum < best_sum*0.5:
                    best_sum = next_line_sum
                    best_row_offset = next_line_offset
                    next_line_offset += 1
                    tol_count = 0
                else:
                    tol_count += 1
                    next_line_offset += 1
                    if tol_count > tolerance:
                        break
            row['bbs'][1] = best_row_offset
            if row['bbs'][3] < row['bbs'][1]:
                row['bbs'][3] = row['bbs'][1]+2
        return rows        
        
class TableNet:
    '''
    Class này sử dụng cho dạng full-border
    Model hiện tại đang sử dụng là kiến trúc mạng Unet
    refers repo: https://github.com/chineseocr/table-ocr
    @model: model_line
    @img: ảnh đầu vào
    @size: kích thước đầu vào
    @engine: core ocr <easyocr|akaocr>
    @reader: ocr 
    '''
    def __init__(self, model_weights, engine, reader, size = (1024,1024)):
        model = table_net((None, None, 3), 2)
        model.load_weights(model_weights)
        self.model = model
        self.size = size
        self.engine = engine
        self.reader = reader

    def infer(self, img):
        #processing img
        sizew, sizeh = self.size
        inputBlob, fx, fy = letterbox_image(img[..., ::-1], (sizew, sizeh))
        hprob=0.5 #0.5
        vprob=0.5 #0.5
        # print(f'input shape: {np.array([np.array(inputBlob) / 255.0]).shape}')
        pred = self.model.predict(np.expand_dims(np.array(inputBlob) / 255.0, axis=0))
        # print(f'output shape: {pred.shape}')
        #rows = 0, cols = 1
        pred = pred[0]
        vpred = pred[..., 1] > vprob 
        hpred = pred[..., 0] > hprob 
        vpred = vpred.astype(int)
        hpred = hpred.astype(int)

        #xử lý các đường line sau predict
        row=50 #100
        col=50 #100
        colboxes = get_table_line(vpred, axis=1, lineW=col)
        rowboxes = get_table_line(hpred, axis=0, lineW=row)
        ccolbox = []
        crowlbox = []
        if len(rowboxes) > 0:
            rowboxes = np.array(rowboxes)
            rowboxes[:, [0, 2]] = rowboxes[:, [0, 2]] / fx
            rowboxes[:, [1, 3]] = rowboxes[:, [1, 3]] / fy
            xmin = rowboxes[:, [0, 2]].min()
            xmax = rowboxes[:, [0, 2]].max()
            ymin = rowboxes[:, [1, 3]].min()
            ymax = rowboxes[:, [1, 3]].max()
            ccolbox = [[xmin, ymin, xmin, ymax], [xmax, ymin, xmax, ymax]]
            rowboxes = rowboxes.tolist()

        if len(colboxes) > 0:
            colboxes = np.array(colboxes)
            colboxes[:, [0, 2]] = colboxes[:, [0, 2]] / fx
            colboxes[:, [1, 3]] = colboxes[:, [1, 3]] / fy

            xmin = colboxes[:, [0, 2]].min()
            xmax = colboxes[:, [0, 2]].max()
            ymin = colboxes[:, [1, 3]].min()
            ymax = colboxes[:, [1, 3]].max()
            colboxes = colboxes.tolist()
            crowlbox = [[xmin, ymin, xmax, ymin], [xmin, ymax, xmax, ymax]]
        rowboxes += crowlbox
        colboxes += ccolbox

        alph = 3
        rboxes_row_, rboxes_col_ = adjust_lines(rowboxes, colboxes, alph=alph)
        rowboxes += rboxes_row_
        colboxes += rboxes_col_

        nrow = len(rowboxes)
        ncol = len(colboxes)
        for i in range(nrow):
                for j in range(ncol):
                    rowboxes[i] = line_to_line(rowboxes[i], colboxes[j], 500)
                    colboxes[j] = line_to_line(colboxes[j], rowboxes[i], 500)

        tmp = np.zeros(img.shape[:2], dtype='uint8')
        tmp = draw_lines(tmp, rowboxes + colboxes, color=255, lineW=4)
        labels = measure.label(tmp < 255, connectivity=2)  
        regions = measure.regionprops(labels)
        ceilboxes = minAreaRectbox(regions, False, tmp.shape[1], tmp.shape[0], True, True)
        boxes = minAreaRectbox_v2(regions, False, tmp.shape[1], tmp.shape[0], True, True)
        ceilboxes = np.array(ceilboxes)
        img_,_= draw_boxes_v2(img,ceilboxes)
        return ceilboxes,boxes,img_

        return ceilboxes,boxes

    def ocr(self, boxes, big_table_img, fraction, img, output_dir='demo', debug=True):
        # #infer table lines
        # ceilboxes,boxes = self.infer(img)
        # sortbbox = Sortlayer()
        # ceilboxes,rows = sortbbox(boxes)

        # outer = []
        # ls = []

        # #infer character on whole page
        # outs = self.reader.detect_and_recognize(img)
        # list_outs = list(outs)

        # #loop and match
        # for box in ceilboxes:
        #     x1_, y1_, x2_, y2_, x3_, y3_, x4_, y4_ = box[0][0],box[0][1],box[1][0],box[1][1],box[2][0],box[2][1],box[3][0],box[3][1]
        #     x1, y1, x2, y2, x3, y3, x4, y4 = int(x1_),int(y1_),int(x2_),int(y2_),int(x3_),int(y3_),int(x4_),int(y4_)
        #     top_left_x = min([x1,x2,x3,x4])
        #     top_left_y = min([y1,y2,y3,y4])
        #     bot_right_x = max([x1,x2,x3,x4])
        #     bot_right_y = max([y1,y2,y3,y4])
            
        #     sub_img = img[top_left_y:bot_right_y, top_left_x:bot_right_x]
        #     new_w,new_h,c = sub_img.shape
        
        #     if new_h > 30 and new_w >=16:
        #         ls.append([[x1_, y1_],[ x2_, y2_],[x3_, y3_],[x4_, y4_]])
        #         sub_word = []
        #         for result in list_outs:
        #             x1,y1,x2,y2 = result[0]
        #             x_center, y_center = (x1+x2)/2, (y1+y2)/2
        #             # print("Text:", result[1])
        #             if x_center > top_left_x and y_center > top_left_y and x_center < bot_right_x and y_center < bot_right_y:
        #                 sub_word.append(result[1])
        #         out = ''
        #         for i, result in enumerate(sub_word):
        #             if i == 0:
        #                 out += result
        #             else:
        #                 out += ' ' + result
        #         outer.append(out)
        #         print("Out:", out)

        # # dataframe = to_csv(outer,rows,ls)
        # arr = np.array(outer)

        # count = 0
        # for i in rows[0]:
        #     if i in ls:
        #         count +=1

        # try:
        #     arr = arr.reshape(len(arr)//count,count)
        #     # dataframe = pd.DataFrame(arr)
        #     # dataframe.to_csv('/home/ec2-user/pipeline_table_extraction/output/csv/test1.csv', index=False)
        #     return arr
        # except:
        #     print('ERROR: CAN NOT RESHAPE TO TABLE!!!')
        #     return arr

        #------------------------------------------------------------------------------------
        #sắp xếp lại bbox của từng ceil
        sortbbox = Sortlayer()
        ceilboxes,rows = sortbbox(boxes)

        outer = []
        ls = []
        for idx, box in enumerate(ceilboxes):
            x1_, y1_, x2_, y2_, x3_, y3_, x4_, y4_ = box[0][0],box[0][1],box[1][0],box[1][1],box[2][0],box[2][1],box[3][0],box[3][1]
            x1, y1, x2, y2, x3, y3, x4, y4 = int(x1_),int(y1_),int(x2_),int(y2_),int(x3_),int(y3_),int(x4_),int(y4_)
            top_left_x = min([x1,x2,x3,x4])
            top_left_y = min([y1,y2,y3,y4])
            bot_right_x = max([x1,x2,x3,x4])
            bot_right_y = max([y1,y2,y3,y4])
            sub_img = img[top_left_y:bot_right_y, top_left_x:bot_right_x]
            new_w,new_h,c = sub_img.shape
            sub_img = big_table_img[int(top_left_y/fraction):int(bot_right_y/fraction), int(top_left_x/fraction):int(bot_right_x/fraction)]
            if new_h > 30 and new_w >=16:
                ls.append([[x1_,y1_],[x2_,y2_],[x3_,y3_],[x4_,y4_]])
                # ls.append([[x1_/fraction,y1_/fraction],[x2_/fraction,y2_/fraction],[x3_/fraction,y3_/fraction],[x4_/fraction,y4_/fraction]])
                if debug:
                    cv2.imwrite(os.path.join(output_dir, f'tablenet_cell_{idx}.jpg'), sub_img)
                # Remove horizontal and verticle lines from cell image
                sub_img = remove_lines(sub_img)
                results = self.reader.readtext(sub_img, mag_ratio=2, text_threshold=0.5, low_text=0.3)
                
                
                plain_text = ""
                # print(results)
                for box in results:
                    plain_text += box[-2]
                    plain_text += " "
                
                # Post-processing output
                plain_text = postprocess_ocr_output(plain_text)
                
                # print(plain_text)
                outer.append(plain_text)
            # else:
            #     print('yooo WTFFFFF')

        arr = np.array(outer) 

        count = 0
        for i in rows[0]:
            if i in ls:
                count +=1

        try:
            arr = arr.reshape(len(arr)//count,count)
            # dataframe = pd.DataFrame(arr)
            # dataframe.to_csv('/home/ec2-user/pipeline_table_extraction/output/csv/test1.csv', index=False)
            return arr, ls
        except:
            print('ERROR: CAN NOT RESHAPE TO TABLE!!!')
            return arr, ls
  
class TablePipeline:
    def __init__(self, det_model, rec_model, tablenet, reader):
        self.det_model = det_model
        self.rec_model = rec_model
        self.tablenet = tablenet
        self.reader = reader
    
    def rescale_img(self, img, fraction=2):
        width = int(img.shape[1] * fraction)
        height = int(img.shape[0] * fraction)
        dim = (width, height)
        
        # resize image
        resized = cv2.resize(img, dim, interpolation = cv2.INTER_AREA)
        return resized
    
    def infer_bordered_table(self, args, big_image, fraction, image, image_copy, det_predictions, table_idx, image_path=None):
        if image_path is None:
            image_path = args.image_path
#         image = cv2.imread(image_path)
        
#         # # Preprocessing input
#         # gray = cv2.cvtColor(image,cv2.COLOR_BGR2GRAY)
#         # #better define borders
#         # kernel = np.ones((3, 3), np.uint8)
#         # gray = cv2.erode(gray, kernel, iterations=1)
#         # gray = cv2.dilate(gray, kernel, iterations=1)
#         # image = cv2.cvtColor(gray, cv2.COLOR_GRAY2RGB)
        
#         h, w, _ = image.shape
#         fraction = 1
        
#         if (w > h and w > 3000 and w < 5000) or (h > w and h > 3000 and h < 5000):
#             fraction = 0.33
#             image = self.rescale_img(image, fraction)
#             print(f'new img shape {image.shape}')
#         elif (w > h and w > 5000 and w < 7000) or (h > w and h > 5000 and h < 7000):
#             fraction = 0.2
#             image = self.rescale_img(image, fraction)
#             print(f'new img shape {image.shape}')
#         elif (w >= 7000) or (h >= 7000):
#             fraction = 0.15
#             image = self.rescale_img(image, fraction)
#             print(f'new img shape {image.shape}') 
            
#         image_copy = np.copy(image)
        h, w, _ = image.shape 
        basename = os.path.basename(image_path)
        filename = '.'.join(basename.split('.')[:-1])
        # det_predictions = self.det_model.infer(args, image)
        # det_predictions = self.det_model.infer(args, image, f'{filename}_det')
        tsr_predictions = []
        table_images = []
        for idx, det_prediction in enumerate(det_predictions):
            xmin, ymin, xmax, ymax = det_prediction['bbs']
            xmin = int(xmin)
            ymin = int(ymin)
            xmax = int(xmax)
            ymax = int(ymax)
            table_img = image_copy[ymin:ymax, xmin:xmax, :]
            big_table_img = big_image[int(ymin/fraction):int(ymax/fraction), int(xmin/fraction):int(xmax/fraction), :]
            table_img_copy = np.copy(table_img)
            timing = {}
            timing['start_rec_infer_time'] = time.time()
            ceilboxes, boxes, img_  = self.tablenet.infer(table_img_copy)
            timing['end_rec_infer_time'] = time.time()
            if args.debug:
                if not os.path.exists(os.path.join(args.output_dir, filename, 'rec', f'table_{table_idx}')):
                    os.makedirs(os.path.join(args.output_dir, filename, 'rec', f'table_{table_idx}'))
                cv2.imwrite(os.path.join(args.output_dir, filename, 'rec', f'table_{table_idx}', 'tablenet_rec.jpg'), img_)
            print('tsr inferred successfully. Start OCR')
            arr, ls = self.tablenet.ocr(boxes, big_table_img, fraction, img_, output_dir=os.path.join(args.output_dir, filename, 'rec', f'table_{table_idx}'), debug=args.debug)
            if args.debug:
                cv2.imwrite(os.path.join(args.output_dir, filename, 'rec', f'table_{table_idx}', 'tablenet_rec_after_line_deletion.jpg'), img_)
            dataframe = pd.DataFrame(arr)
            dataframe.to_excel(os.path.join(args.output_dir, filename, f'{filename}_table_{table_idx}.xlsx'))
            if args.debug:
                print(f'df: {dataframe}')
        print(f'Pipeline run successfully, file saved at: {os.path.join(args.output_dir, filename)}')
        return timing
            
    def infer_borderless_tableTrans(self, args, big_image, fraction, image, image_copy, det_predictions, table_idx, image_path=None):
        if image_path is None:
            image_path = args.image_path
#         image = cv2.imread(image_path)
#         h, w, _ = image.shape
#         fraction = 1
        
#         if (w > h and w > 3000 and w < 5000) or (h > w and h > 3000 and h < 5000):
#             fraction = 0.3
#             image = self.rescale_img(image, fraction)
#             print(f'new img shape {image.shape}')
#         elif (w > h and w > 5000 and w < 7000) or (h > w and h > 5000 and h < 7000):
#             fraction = 0.2
#             image = self.rescale_img(image, fraction)
#             print(f'new img shape {image.shape}')
#         elif (w >= 7000) or (h >= 7000):
#             fraction = 0.15
#             image = self.rescale_img(image, fraction)
#             print(f'new img shape {image.shape}') 
#         else:
#             print('no resizing needed')
            
#         image_copy = np.copy(image)
        h, w, _ = image.shape
        
        basename = os.path.basename(image_path)
        filename = '.'.join(basename.split('.')[:-1])
        print(f'filename: {filename}')
        # det_predictions = self.det_model.infer(args, image)
        tsr_predictions = []
        table_images = []
        small_table_images = []
        accum_rec_infer_time = []
        grids = []
        for idx, det_prediction in enumerate(det_predictions):
            xmin, ymin, xmax, ymax = det_prediction['bbs']
            xmin = int(xmin)
            ymin = int(ymin)
            xmax = int(xmax)
            ymax = int(ymax)
            table_img = image_copy[ymin:ymax, xmin:xmax, :]
            big_table_img = big_image[int(ymin/fraction):int(ymax/fraction), int(xmin/fraction):int(xmax/fraction), :]
            table_img_copy = np.copy(table_img)
            timing = {}
            start_rec_infer_time = time.time()
            predictions = self.rec_model.infer(args, table_img_copy, f'{filename}_table_{table_idx}', timing=timing)
            end_rec_infer_time = time.time()
            # timing['start_rec_infer_time'] = start_rec_infer_time
            # timing['end_rec_infer_time'] = end_rec_infer_time
            
            # print(predictions)
            accum_rec_infer_time.append(time.time()-start_rec_infer_time)
            tsr_predictions.append(predictions)
            # table_images.append(table_img)
            table_images.append(big_table_img)
            small_table_images.append(table_img)
            # grids.append(grid)
            
        # print(f"AVG rec infer time: {np.mean(accum_rec_infer_time)}; Total rec infer time: {np.sum(accum_rec_infer_time)}")
        
        for idx, tsr_prediction, table_image, small_table_image in zip(range(len(table_images)), tsr_predictions, table_images, small_table_images):
            grid = self.pred2df(tsr_prediction, table_image, small_table_image, fraction, output_dir=os.path.join(args.output_dir, filename, 'rec', f'table_{table_idx}'), debug=args.debug)
            df = pd.DataFrame(grid)
            if args.debug:
                print(f'df: {df}')
            df.to_csv(os.path.join(args.output_dir, filename, f'borderless_table_{table_idx}.csv'), index=False, encoding='utf-8-sig')
            df.to_excel(os.path.join(args.output_dir, filename, f'borderless_table_{table_idx}.xlsx'), encoding='utf-8-sig')
        print(f'Pipeline run successfully, file saved at: {os.path.join(args.output_dir, filename)}')
        return timing
    
    def quantize_model(self, input_dir, output_dir, filename):
        # Get all image files in directory 
        files = os.listdir(input_dir)
        
        # Filter for only image files
        image_files = [f for f in files if f.endswith(('.jpg', '.jpeg', '.png'))]

        # Create empty list to hold tensors 
        image_tensors = []

        # Iterate through each image 
        for image_file in image_files:

            # Read in image using torch
            image = read_image(os.path.join(input_dir, image_file))

            # Check if grayscale and convert to RGB
            if len(image.shape) == 2: 
              image = torch.stack((image,)*3, dim=0)

            # Add image tensor to list 
            image_tensors.append(image)

        # Stack list of tensors into one big tensor 
        cal_inputs = torch.stack(image_tensors).to('cuda')

        self.det_model.quantize(calibration_inputs=cal_inputs, output_dir=output_dir, filename=filename)
        
    def infer(self, args, image_path=None):        
        # Detect table
        if image_path is None:
            image_path = args.image_path
        image = cv2.imread(image_path)
        
        start_infer_time = time.time()
        image_filename = '.'.join(os.path.basename(image_path).split('.')[:-1])
        big_image = image.copy()
        h, w, _ = image.shape
        fraction = 1
        
        if (w > h and w > 2000 and w < 3000) or (h > w and h > 2000 and h < 3000):
            fraction = 0.4
            image = self.rescale_img(image, fraction)
            print(f'new img shape {image.shape}')
        elif (w > h and w >= 3000 and w < 5000) or (h > w and h >= 3000 and h < 5000):
            fraction = 0.3
            image = self.rescale_img(image, fraction)
            print(f'new img shape {image.shape}')
        elif (w > h and w >= 5000 and w < 7000) or (h > w and h >= 5000 and h < 7000):
            fraction = 0.2
            image = self.rescale_img(image, fraction)
            print(f'new img shape {image.shape}')
        elif (w >= 7000) or (h >= 7000):
            fraction = 0.15
            image = self.rescale_img(image, fraction)
            print(f'new img shape {image.shape}') 
        else:
            print('no resizing needed')
            
        image_copy = np.copy(image)
        h, w, _ = image.shape
        if not os.path.exists(os.path.join(args.output_dir, image_filename)):
            os.makedirs(os.path.join(args.output_dir, image_filename))
        timing = {}
        start_det_infer_time = time.time()
        det_predictions = self.det_model.infer(args, image, image_filename, timing=timing)
        end_det_infer_time = time.time()
        # timing['start_det_infer_time'] = start_det_infer_time
        # timing['end_det_infer_time'] = end_det_infer_time
        timing['det_infer_time'] = timing['end_det_infer_time'] - timing['start_det_infer_time']
        
        timing['rec_infer_time'] = 0
        for table_idx, det_prediction in enumerate(det_predictions):
            xmin, ymin, xmax, ymax = det_prediction['bbs']
            xmin = int(xmin)
            ymin = int(ymin)
            xmax = int(xmax)
            ymax = int(ymax)
            table_img = image_copy[ymin:ymax, xmin:xmax, :]
            # Classify table
            # if is_full_border(args.image_path, output_path=None):
            if is_full_border_image(table_img, output_path=None):
                print('FULL BORDER')
                timing_ = self.infer_bordered_table(args, big_image, fraction, image, image_copy, [det_prediction], table_idx, image_path)
                timing['rec_infer_time'] += timing_['end_rec_infer_time'] - timing_['start_rec_infer_time']
            else:
                print('BORDERLESS')
                timing_ = {}
                timing_ = self.infer_borderless_tableTrans(args, big_image, fraction, image, image_copy, [det_prediction], table_idx, image_path)
                timing['rec_infer_time'] += timing_['end_rec_infer_time'] - timing_['start_rec_infer_time']
        timing['infer_time'] = time.time() - start_infer_time
        return timing
    
    def pred2df2(self, tsr_prediction, image, grid):
        grid = np.empty_like(grid, dtype=str)
        for cell in tsr_prediction:
            xmin, ymin, xmax, ymax = cell['bbs']
            start_row, start_col, end_row, end_col = cell['start_row'], cell['start_col'], cell['end_row'], cell['end_col']
            cell_img = image[ymin:ymax, xmin:xmax, :]
            content = self.reader.readtext(cell_img, detail = 0, mag_ratio=2, text_threshold=0.3, low_text=0.1)
            content = ' '.join(content)
            grid[start_row: end_row, start_col:end_col].fill('')
            grid[start_row, start_col] = content
            
        return grid
            
    def pred2df(self, tsr_prediction, image, small_table_image, fraction, output_dir='demo', debug=True):
        if debug and not os.path.exists(output_dir):
            os.makedirs(output_dir)
        if debug:
            image_copy = np.copy(image)
        rows = []
        cols = []
        spanning_cells = []
        for entity in tsr_prediction:
            if entity['label'] in ['table row', 'column header']:
                rows.append({
                    'bbs': [int(coor/fraction) for coor in entity['bbs']],
                    'type': entity['label']
                })
            elif entity['label'] in ['table column']:
                cols.append({
                    'bbs': [int(coor/fraction) for coor in entity['bbs']],
                    'type': entity['label']
                })
            elif entity['label'] in ['table spanning cell', 'table projected row header']:
                spanning_cells.append({
                    'bbs': [int(coor/fraction) for coor in entity['bbs']],
                    'type': entity['label']
                })
        
        rows = sorted(rows, key=lambda dct: dct['bbs'][1])
        cols = sorted(cols, key=lambda dct: dct['bbs'][0])
        h, w, _ = image.shape
            
        # Remove noisy rows
        del_row_ids = []
        curr_row_id = 0
        row_id = 0
        if len(rows) > 50:
            min_gap = h/70
        elif len(rows) > 30:
            min_gap = h/60
        else:
            min_gap = h/50
        while row_id < len(rows) and curr_row_id < len(rows):
            if row_id == 0:
                row_id += 1
                continue
            if abs(rows[curr_row_id]['bbs'][1] - rows[row_id]['bbs'][1]) <  min_gap:
                if np.sum(image[rows[curr_row_id]['bbs'][1],:,:]) < np.sum(image[rows[row_id]['bbs'][1],:,:]):
                    del_row_ids.append(row_id)
                else:
                    del_row_ids.append(curr_row_id)
                curr_row_id = row_id + 1
                row_id = row_id + 2
            else:
                curr_row_id = row_id
                row_id = row_id + 1
        for del_row_id in del_row_ids[::-1]:
            del rows[del_row_id]
            
        # Remove noisy cols
        del_col_ids = []
        curr_col_id = 0
        col_id = 0
        while col_id < len(cols) and curr_col_id < len(cols):
            if col_id == 0:
                col_id += 1
                continue
            if abs(cols[curr_col_id]['bbs'][0] - cols[col_id]['bbs'][0]) <  w/30:
                del_col_ids.append(col_id)
                curr_col_id = col_id + 1
                col_id = col_id + 2
            else:
                curr_col_id = col_id
                col_id = col_id + 1
        for del_col_id in del_col_ids[::-1]:
            del cols[del_col_id]
        
        grid = np.full((len(rows), len(cols)), fill_value=-1)
        # Autoincremental value of cell_id
        cell_id = 0
        def find_col_id(x):
            min_dist = sys.float_info.max
            col_id = 0
            for idx, col in enumerate(cols):
                if abs(col['bbs'][0] - x) < min_dist:
                    min_dist = abs(col['bbs'][0] - x)
                    col_id = idx
                if idx == len(cols) - 1:
                    if abs(col['bbs'][2] - x) < min_dist:
                        min_dist = abs(col['bbs'][2] - x)
                        col_id = idx + 1
            return col_id

        def find_row_id(y):
            min_dist = sys.float_info.max
            row_id = 0
            for idx, row in enumerate(rows):
                if abs(row['bbs'][1] - y) < min_dist:
                    min_dist = abs(row['bbs'][1] - y)
                    row_id = idx
                if idx == len(rows) - 1:
                    if abs(row['bbs'][3] - y) < min_dist:
                        min_dist = abs(row['bbs'][3] - y)
                        row_id = idx + 1
            return row_id

        cells_dict = {}
        for spanning_cell in spanning_cells:
            x1 = spanning_cell['bbs'][0]
            y1 = spanning_cell['bbs'][1]
            x2 = spanning_cell['bbs'][2]
            y2 = spanning_cell['bbs'][3]
            start_col_id = find_col_id(x1)
            end_col_id = find_col_id(x2)
            
            start_row_id = find_row_id(y1)
            end_row_id = find_row_id(y2)
            
            # print('col', start_col_id, end_col_id)
            # print('row', start_row_id, end_row_id)
            grid[start_row_id:end_row_id, start_col_id:end_col_id].fill(cell_id)
            cells_dict[cell_id] = {
                                   'start_row_id': start_row_id,
                                   'end_row_id': end_row_id,
                                   'start_col_id': start_col_id,
                                   'end_col_id': end_col_id
                                   }
            cell_id += 1
        
        for row_id in range(grid.shape[0]):
            for col_id in range(grid.shape[1]):
                if grid[row_id, col_id] == -1:
                    grid[row_id, col_id] = cell_id
                    cells_dict[cell_id] = {
                                   'start_row_id': row_id,
                                   'end_row_id': row_id + 1,
                                   'start_col_id': col_id,
                                   'end_col_id': col_id + 1
                                   }
                    cell_id += 1
        
        if debug:
            print('--------------------')
            print('GRID:')
            print(grid)
            print('--------------------')
        grid = grid.astype('str')
        
        # Refine rows seperator
        if fraction == 1:
            tolerance = 3
        elif 0.2 < fraction and fraction < 1:
            tolerance = 5
        elif 0.15 < fraction and fraction <= 0.2:
            tolerance = 6
        elif 0 < fraction and fraction <= 0.15:
            tolerance = 7
            
        print(f'tolerance: {tolerance}')
        rows = self.rec_model.refine_recog_rows(image, rows, tolerance=tolerance)
        
        for key in cells_dict:
            start_row_id = cells_dict[key]['start_row_id']
            start_col_id = cells_dict[key]['start_col_id']
            end_row_id = cells_dict[key]['end_row_id']
            end_col_id = cells_dict[key]['end_col_id']
            
            start_row_offset = rows[start_row_id]['bbs'][1]
            try:
                end_row_offset = rows[end_row_id]['bbs'][1]
            except:
                end_row_offset = rows[end_row_id-1]['bbs'][3]
            if end_row_offset == start_row_offset:
                try:
                    end_row_offset = rows[start_row_id+1]['bbs'][1]
                except:
                    end_row_offset = h
                
            start_col_offset = cols[start_col_id]['bbs'][0]
            try:
                end_col_offset = cols[end_col_id]['bbs'][0]
            except:
                end_col_offset = cols[end_col_id-1]['bbs'][2]
            
            # canvas = np.full(((end_row_offset-start_row_offset)*3, (end_col_offset-start_col_offset)*3, 3), (255, 255, 255))
            # canvas = np.full_like(image, (255, 255, 255))
            # canvas[:end_row_offset-start_row_offset, :end_col_offset-start_col_offset, :] = image[start_row_offset:end_row_offset, start_col_offset: end_col_offset, :]
            
            cell_img = image[start_row_offset:end_row_offset, start_col_offset: end_col_offset, :]
            # Remove horizontal and verticle lines
            cell_img = remove_lines(cell_img)
            
            if debug:
                cv2.rectangle(small_table_image, (ceil(start_col_offset*fraction), ceil(start_row_offset*fraction)), (ceil(end_col_offset*fraction), ceil(end_row_offset*fraction)), (0, 255, 0), 1)
                # cv2.rectangle(image_copy, (start_col_offset, start_row_offset), (end_col_offset, end_row_offset), (0, 255, 0), 2)
                cv2.imwrite(os.path.join(output_dir, f'tableTrans_cell_{key}.jpg'), cell_img)
            # cell_img = self.rescale_img(cell_img, 3)
            # cv2.imwrite(f'demo/cell_{key}.jpg', cell_img)
            # cell_img = cv2.cvtColor(cell_img, cv2.COLOR_BGR2GRAY)
            
            # custom_oem_psm_config = r'--oem 3 --psm 6 -l eng+vie'
            # content = pytesseract.image_to_string(image, config=custom_oem_psm_config)
            # print(f'tesseract content: {content}')
            content = self.reader.readtext(cell_img, detail = 0, min_size=5, mag_ratio=4, text_threshold=0.5, low_text=0.2, link_threshold=0.3)
            content = ' '.join(content)
            # Post-processing output
            content = postprocess_ocr_output(content)
            
            grid[start_row_id: end_row_id, start_col_id:end_col_id].fill('')
            grid[start_row_id, start_col_id] = content
            
        if debug:
            cv2.imwrite(os.path.join(output_dir, f'tableTrans_table_rec.jpg'), small_table_image)
            cv2.imwrite(os.path.join(output_dir, f'tableTrans_table_after_line_deletion.jpg'), image)
            print('--------------------')
            print('GRID:')
            print(grid)
            print('--------------------')
        return grid


def get_class_map():
    class_map = {
        0: "table",
        1: "table column",
        2: "table row",
        3: "table column header",
        4: "table projected row header",
        5: "table spanning cell",
        6: "no object",
    }
    return class_map
    

def main():
    cmd_args = get_args().__dict__
    det_config_args = json.load(open(cmd_args['det_config_file'], 'rb'))
    for key, value in cmd_args.items():
        if not key in det_config_args or not value is None:
            det_config_args[key] = value
    #config_args.update(cmd_args)
    det_args = type('Args', (object,), det_config_args)
    
    rec_config_args = json.load(open(cmd_args['rec_config_file'], 'rb'))
    for key, value in cmd_args.items():
        if not key in rec_config_args or not value is None:
            rec_config_args[key] = value
    #config_args.update(cmd_args)
    rec_args = type('Args', (object,), rec_config_args)
    # print(det_args.__dict__)
    # print('-' * 100)

    print("loading model")
    device = torch.device(det_args.device)
    
    # Load detection model
    det_args.config_file = det_args.det_config_file
    det_args.model_load_path = det_args.det_model_load_path
    det_transformer = TableTransformer(det_args, device, get_class_map(), 'det')
    
    # Rec detection model
    rec_args.config_file = rec_args.rec_config_file
    rec_args.model_load_path = rec_args.rec_model_load_path
    rec_transformer = TableTransformer(rec_args, device, get_class_map(), 'rec')
    
    #Reader
    lang_list = []
    if 'en' in cmd_args['lang']:
        lang_list.append('en')
    if 'vi' in cmd_args['lang']:
        lang_list.append('vi')
    reader = easyocr.Reader(lang_list)
    
    # TableNet
    tablenet = TableNet('/home/cv/pipeline_table/tablenet/table_line_models/table-line.h5',
                        'easyocr',
                        reader,
                        (1024,1024))
    
    # Pipeline
    table_pipeline = TablePipeline(det_transformer, rec_transformer, tablenet, reader)
    
    # # Quantize
    # table_pipeline.quantize_model(input_dir='/home/cv/pipeline_table/sample_data_pipeline/untrained_uncropped_borderless',
    #                        output_dir='/workspace/khoi_huan/quantized_models/det',
    #                        filename='td_f_only_05.pt')
    
    
    # Infer
    if os.path.isdir(rec_args.image_path):
        pipeline_timing = {}
        pipeline_timing['infer_time'] = 0
        pipeline_timing['det_infer_time'] = 0
        pipeline_timing['rec_infer_time'] = 0
        pipeline_timing['det_rec_time'] = 0
        count = 0
        for filename in os.listdir(rec_args.image_path):
            if filename.endswith(".jpg") or filename.endswith(".png"):
                try:
                    file_path = os.path.join(rec_args.image_path, filename)
                    print(file_path)
                    start_time = time.time()
                    timing = table_pipeline.infer(rec_args, file_path)
                    # pipeline_timing['infer_time'] += time.time()-start_time
                    pipeline_timing['infer_time'] += timing['infer_time']
                    pipeline_timing['det_infer_time'] += timing['det_infer_time']
                    pipeline_timing['rec_infer_time'] += timing['rec_infer_time']
                    count += 1
                except Exception as e:
                    print(f'Exception: {e}')
                
        pipeline_timing['det_rec_time'] = pipeline_timing['det_infer_time'] + pipeline_timing['rec_infer_time']
        for key in pipeline_timing.keys():
            pipeline_timing[key] = pipeline_timing[key]/count
        print(f'average pipeline runtime: {pipeline_timing}')
    elif os.path.isfile(rec_args.image_path):
        table_pipeline.infer(rec_args)
        # table_pipeline.infer_bordered_table(rec_args)
    else:
        raise FileNotFoundError(f"file or folder {det_args.image_path} not found")


if __name__ == "__main__":
    main()