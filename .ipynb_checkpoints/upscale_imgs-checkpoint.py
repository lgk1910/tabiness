import os 
from PIL import Image
import random

input_dir = 'sample_data_pipeline/untrained_uncropped_bordered' 
output_dir = 'sample_data_pipeline/untrained_uncropped_bordered_3_5_times_upscaled'

if not os.path.exists(output_dir):
    os.makedirs(output_dir)

for filename in os.listdir(input_dir):
    if filename.endswith('.jpg') or filename.endswith('.png') or filename.endswith('.bmp'):
        img = Image.open(os.path.join(input_dir, filename))
        width, height = img.size
        factor = random.uniform(3, 5)  
        resized = img.resize((int(width*factor), int(height*factor)))
        resized.save(os.path.join(output_dir, filename))